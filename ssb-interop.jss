document.addEventListener('DOMContentLoaded', function() {
  let tt__customCss = `.menu ul li a:not(.inline_menu_link) {color: #fff !important;} [lang] body{font-weight:600;}`
  $.ajax({
      url: "https://jahabi.gitlab.io/slack-night-mode/black.css",
      success: function(css) {
          $("<style></style>").appendTo('head').html(css + tt__customCss);
      }
  });
});
